import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { BlogService } from '../blog.service';

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewComponent implements OnInit {
  postForm = this.fb.group({
    title: ['', [Validators.required,
                Validators.minLength(1)]],
    content: ['', [Validators.required,
                  Validators.minLength(1)]],
  });

  constructor(
    private blogService: BlogService,
    private router: Router,
    private fb: FormBuilder,
    public snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.blogService.postPost(this.postForm.value.title, this.postForm.value.content).subscribe( (returnedPost) => {
      this.snackbar.open('Post added', 'Dismiss', {
        duration: 3000
      });
      this.router.navigate(['/posts']);
    }, (error) => {
      this.snackbar.open(error.error.message, 'Dismiss', {
        duration: 3000
      });
    });
  }
}
