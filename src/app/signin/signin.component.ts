import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { BlogService } from '../blog.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {
  profileForm = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required,
                    Validators.pattern(/^[\d\S]*\d[\d+\S]*$/),
                    Validators.minLength(4)]],
  });

  constructor(
    private blogService: BlogService,
    private router: Router,
    private fb: FormBuilder,
    public snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.blogService.signin(this.profileForm.value.username, this.profileForm.value.password).subscribe( (returnedAuthor) => {
      this.snackbar.open('Welcome!', 'Dismiss', {
        duration: 3000
      });
      this.router.navigate(['/posts']);
    }, (error) => {
      this.snackbar.open(error.error.message, 'Dismiss', {
        duration: 3000
      });
    });
  }
}
