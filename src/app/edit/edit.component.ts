import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Post } from '../models/post';

import { BlogService } from '../blog.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  id: string;
  post: Post;
  postForm = this.fb.group({
    title: ['', Validators.required],
    content: ['', Validators.required],
  });

  constructor(
    private blogService: BlogService,
    private router: Router,
    private fb: FormBuilder,
    public snackbar: MatSnackBar,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get('id');
    this.blogService.getPost(this.id).subscribe((post: Post) => {
      this.post = post;
      if (this.post.authorId !== this.blogService.getAuthor().id) {
        this.router.navigate(['/posts']);
        return;
      }
      this.postForm = this.fb.group({
        title: [post.title, Validators.required],
        content: [post.content, Validators.required],
      });
    });
  }

  onSubmit() {
    this.blogService.editPost(this.id, this.postForm.value.title, this.postForm.value.content).subscribe( (returnedPost) => {
      this.snackbar.open('Post edited', 'Dismiss', {
        duration: 3000
      });
      this.router.navigate(['/posts']);
    }, (error) => {
      this.snackbar.open(error.error.message, 'Dismiss', {
        duration: 3000
      });
    });
  }
}
