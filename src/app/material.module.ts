import { NgModule } from '@angular/core';
import {
    MatListModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatButtonModule,
    MatTooltipModule,
    MatCardModule,
    MatSnackBarModule,
} from '@angular/material';

@NgModule({
  exports: [
      MatListModule,
      MatIconModule,
      MatInputModule,
      MatFormFieldModule,
      MatToolbarModule,
      MatButtonModule,
      MatTooltipModule,
      MatCardModule,
      MatSnackBarModule,
  ],
})
export class MaterialModule {}
