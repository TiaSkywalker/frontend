import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Post } from 'src/app/models/post';
import { Observable } from 'rxjs';
import { tap, map, catchError, switchMap } from 'rxjs/operators';

interface AuthorI {
  id: number;
  token: string;
}
interface FavoriteI {
  flag: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  private author: AuthorI;
  get isLoggedIn() {
    return this.getAuthor() !== undefined;
  }

  constructor(
    private http: HttpClient,
  ) { }

  getPosts(): Observable<Post[]> {
    const author = this.getAuthor();
    const headers = {};
    if (author) {
      headers['Authorization'] = 'Token ' + (this.author.token);
    }
    return this.http.get<Post[]>('http://localhost:8000/posts', {
      headers: headers,
    });
  }
  getFilteredPosts(title: string): Observable<Post[]> {
    return this.http.get<Post[]>('http://localhost:8000/posts?title=' + title);
  }
  setFavorite(postId: number): Observable<{}>  {
    const author = this.getAuthor();
    const body = {
      favorite: true,
    };
    return this.http.patch<{}>('http://localhost:8000/posts/' + postId, body, {
      headers: {
        'Authorization': 'Token ' + (author ? this.author.token : '')
      }
    });
  }
  deleteFavorite(postId: number) {
    const author = this.getAuthor();
    const body = {
      favorite: false,
    };
    return this.http.patch<{}>('http://localhost:8000/posts/' + postId, body, {
      headers: {
        'Authorization': 'Token ' + (author ? this.author.token : '')
      }
    });
  }
  getPost(id: string): Observable<Post> {
    return this.http.get<Post>('http://localhost:8000/posts/' + id);
  }
  postPost(title: string, content: string): Observable<Post> {
    const body = {
      title: title,
      content: content,
    };
    const author = this.getAuthor();
    return this.http.post<Post>('http://localhost:8000/posts', body, {
      headers: {
        'Authorization': 'Token ' + (author ? author.token : '')
      }
    });
  }
  editPost(id: string, title: string, content: string): Observable<Post> {
    const body = {
      title: title,
      content: content,
    };
    const author = this.getAuthor();
    return this.http.patch<Post>('http://localhost:8000/posts/' + id, body, {
      headers: {
        'Authorization': 'Token ' + (author ? author.token : '')
      }
    });
  }
  deletePost(id: number): Observable<{}> {
    const author = this.getAuthor();
    return this.http.delete<{}>('http://localhost:8000/posts/' + id, {
      headers: {
        'Authorization': 'Token ' + (author ? this.author.token : '')
      }
    });
  }

  signin(username: string, password: string): Observable<boolean> {
    const body = {
      username: username,
      password: password,
    };
    return this.http.post<{ id: number, token: string }>('http://localhost:8000/register', body).pipe(
      map((_) => true),
      switchMap((_) => this.login(username, password)),
    );
  }
  login(username: string, password: string): Observable<boolean> {
    const body = {
      username: username,
      password: password,
    };
    return this.http.post<{ id: number, token: string }>('http://localhost:8000/login', body).pipe(
      tap((resp) => this.setAuthor(resp.id, resp.token)),
      map((_) => true)
    );
  }
  logout() {
    this.author = null;
    localStorage.removeItem('activeUser');
  }
  setAuthor(id: number, token: string) {
    this.author = {
      id: id,
      token: token,
    };
    localStorage.setItem('activeUser', JSON.stringify(this.author));
  }
  getAuthor(): AuthorI {
    if (!this.author) {
      const json = localStorage.getItem('activeUser');
      const obj = JSON.parse(json);
      if (obj) {
        this.author = {
          id: obj.id,
          token: obj.token
        };
      }
    }
    return this.author;
  }
}
