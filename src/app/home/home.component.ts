import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { FormControl } from '@angular/forms';

import { Post } from '../models/post';
import { BlogService } from '../blog.service';
import { Favorite } from '../models/favorite';
import { debounceTime, switchMap, tap, distinctUntilChanged, filter } from 'rxjs/operators';
import { Observable, merge } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  posts: Post[];
  favorites: Favorite[];
  author: any;
  term: string;
  searchField: FormControl;
  searches: string[] = [];

  constructor(
    private blogService: BlogService,
    private router: Router,
    public snackbar: MatSnackBar,
  ) {
    this.author = this.blogService.getAuthor();
  }

  ngOnInit() {
    this.blogService.getPosts().subscribe((posts) => {
      this.posts = posts;
    }, (error) => {
      this.snackbar.open('Connection error', 'Dismiss');
    });
    this.searchField = new FormControl();

    const newValue = this.searchField.valueChanges.pipe(
      debounceTime(400),
      distinctUntilChanged(),
    );

    const newSearch = newValue.pipe(
      filter((value) => value),
      switchMap((term) => this.searchPosts(term)),
    );

    const noSearch = newValue.pipe(
      filter((value) => !value),
      switchMap((term) => this.blogService.getPosts()),
    );

    merge(noSearch, newSearch).subscribe((filteredPosts) => {
      this.posts = filteredPosts;
    }, (error) => {
      this.snackbar.open('Connection error', 'Dismiss');
    });
  }

  setFavorite(postId: number) {
    this.blogService.setFavorite(postId).subscribe((_) => {
      const p = this.posts.find((post) => post.id === postId);
      p.favorite = true;
    });
  }
  deleteFavorite(postId: number) {
    this.blogService.deleteFavorite(postId).subscribe((_) => {
      const p = this.posts.find((post) => post.id === postId);
      p.favorite = false;
    });
  }
  searchPosts(title: string): Observable<Post[]> {
    return this.blogService.getFilteredPosts(title);
  }
  deletePost(id: number) {
    this.blogService.deletePost(id).subscribe( (returnedPost) => {
      this.snackbar.open('Post deleted', 'Dismiss', {
        duration: 3000
      });
      this.router.navigate(['/home']);
    }, (error) => {
      this.snackbar.open('Post not deleted beacuse of ' + error.error.message, 'Dismiss', {
        duration: 3000
      });
    });
  }
}
