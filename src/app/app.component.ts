import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { BlogService } from './blog.service';
import 'hammerjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'WhatWapp Blog!';
  author: any;

  constructor(
    private blogService: BlogService,
    private router: Router,
    public snackbar: MatSnackBar,
  ) {
  }

  public getAuthor() {
    return this.blogService.getAuthor();
  }
  logoutAuthor() {
    this.blogService.logout();
    this.snackbar.open('Goodbye!', 'Dismiss', {
      duration: 3000
    });
    this.router.navigate(['/home']);
  }
}
