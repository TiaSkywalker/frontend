import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { BlogService } from '../blog.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  profileForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(
    private blogService: BlogService,
    private router: Router,
    private fb: FormBuilder,
    public snackbar: MatSnackBar,
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.blogService.login(this.profileForm.value.username, this.profileForm.value.password).subscribe( (returnedAuthor) => {
      this.snackbar.open('Welcome back!', 'Dismiss', {
        duration: 3000
      });
      this.router.navigate(['/posts']);
    }, (error) => {
      this.snackbar.open(error.error.message, 'Dismiss', {
        duration: 3000
      });
    });
  }
}
