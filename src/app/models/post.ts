export class Post {
    timestamp: Date;

    constructor(
        public id: number,
        public title: string,
        public authorId: number,
        public authorUsername: string,
        timestamp: string,
        public content: string,
        public favorite: boolean,
    ) {
        this.timestamp = new Date(timestamp);
    }
}
