export class Author {
    constructor(
        public id: number,
        public username: string,
    ) {}
}
